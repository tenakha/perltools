#!/usr/bin/perl -w

use strict;
use warnings;

#dotplot.pl -- reads two sequences and prints dotplot

#format of input data: (if any input line contains a #, the program
#                       will ignore the # and everything to the right)                   
#    line 1   title of dotplot
#    line 2   two integers specifying window and threshold
#    line 3   title of first sequence
#    lines 4 through a line ending in *    first sequence
#    next line   title of second sequence
#    following lines, until a line ending in *     second sequence
 
# read input

$/ = "";                           # set switch to read entire paragraph
$_ = <DATA>; $_ =~ s/#(.*)\n/\n/g; # kill comments (introduced by #)
                                   #      at right of each line
  
# split input into lines and parse them
$_ =~ /^(.*)\n\s*(\d+)\s+(\d+)\s*\n(.*)\n([A-Z\n]*)\*\s*\n(.*)\n([A-Z\n]*)\*/;

#extract title, window size and threshold
my $title = $1; my $nwind = $2; my $thresh = $3; 

#extract sequences
#$seqt1 = $4; $seq1 = $5; $seqt2 = $6; $seq2 = $7;
my $seq1 = $5; my $seq2 = $7;

#collect sequences by deleting end-of-line characters
$seq1 =~ s/\n//g; $seq2 =~ s/\n//g; my $n = length($seq1); my $m = length($seq2);

# print postscript header

print <<EOF;

/s /stroke load def /l /lineto load def /m /moveto load def 
/r /rlineto load def /n /newpath load def /c /closepath load def 
/f /fill load def
1.75 setlinewidth 30 30 translate /Helvetica findfont 16 scalefont setfont
EOF

# print matrix

my $dx = 500.0/$n;  my $mdx = -$dx; my $dy = 500.0/$m;
if ($dy < $dx) {$dx = $dy;} $dy = $dx; my $xmx = $n*$dx; my $ymx = $m*$dx;

print "3 510 m ($title   window = $nwind   threshold = $thresh) show\n";
printf "0 0 m 0 %9.2f l %9.2f %9.2f l %9.2f 0 l c s\n", $ymx,$xmx,$ymx,$xmx;

# loop through both sequences recording runs of identical characters

for (my $k = $nwind - $m + 1; $k < $n - $nwind; $k++) {
   my $i = $k; my $j = 1; if ($k < 1) {$i = 1; my $j = 2 - $k;}

   while ($i <= $n - $nwind && $j <= $m - $nwind) {        
       $_ = (substr($seq1,$i -1,$nwind) ^ substr($seq2,$j -1,$nwind)); 
       my $mismatch = ($_ =~ s/[^\x00]//g);
       if ($mismatch < $thresh) {
           my $xl = ($i - 1)*$dx; my $yb = ($m - $j)*$dy;
           printf "n %9.2f %9.2f m %9.2f 0 r 0 %9.2f r %9.2f 0 r c f\n",
                   $xl,$yb,$dx,$dy,$mdx;
       }
       $i++; $j++;
   }
}

print "showpage\n";                   # end postscript file

# material following __END__ is the input data to the program


__END__
ATPases lamprey / dogfish                  #TITLE
15 6                                       #WINDOW, THRESHOLD
Petromyzon marinus mitochondrion           #SEQUENCE 1
ATGACACTAGATATCTTTGACCAATTTACCTCCCCAACA
ATATTTGGGCTTCCACTAGCCTGATTAGCTATACTAGCCCCTAGCTTA
ATATTAGTTTCACAAACACCAAAATTTATCAAATCTCGTTATCACACACTA
CTTACACCCATCTTAACATCTATTGCCAAACAACTCTTTCTTCCAATAAAC
CAACAAGGGCATAAATGAGCCTTAATTTGTATAGCCTCTATAATATTTATC
TTAATAATTAATCTTTTAGGATTATTACCATATACTTATACACCAACTACC
CAATTATCAATAAACATAGGATTAGCAGTGCCACTATGACTAGCTACTGTC
CTCATTGGGTTACAAAAAAAACCAACAGAAGCCCTAGCCCACTTATTACCA
GAAGGTACCCCAGCAGCACTCATTCCCATATTAATTATCATTGAAACTATT
AGTCTTTTTATCCGACCTATCGCCCTAGGAGTCCGACTAACCGCTAATTTA
ACAGCTGGTCACTTACTTATACAACTAGTTTCTATAACAACCTTTGTAATA
ATTCCTGTCATTTCAATTTCAATTATTACCTCACTACTTCTTCTATTA
CTAACAATTCTGGAGTTAGCTGTTGCTGTAATCCAGGCATATGTATTTATT
CTACTTTTAACTCTTTATCTGCAAGAAAACGTTT*    
Scyliorhinus canicula mitochondrion         #SEQUENCE 2
ATGATTATAAGCTTTTTTGATCAATTCCTAAGTCCCTCCTTTCTAGGA
ATCCCACTAATTGCCCTAGCTATTTCAATTCCATGATTAATATTTCCAACACCAACC
AATCGTTGACTTAATAATCGATTATTAACTCTTCAAGCATGATTTATTAACCGATTTATT
TATCAACTAATACAACCCATAAATTTAGGAGGACATAAATGAGCTATCTTATTTACAGCC
CTAATATTATTTTTAATTACCATCAATCTTCTAGGTCTCCTTCCATATACTTTTACGCCT
ACAACTCAACTTTCTCTTAATATAGCCTTTGCCCTGCCCTTATGGCTTACAACTGTATTA
ATTGGTATATTTAATCAACCAACCATTGCCCTAGGGCACTTATTACCTGAAGGTACCCCA
ACCCCTTTAGTACCAGTACTAATCATTATCGAAACCATCAGTTTATTTATTCGACCATTA
GCCTTAGGAGTCCGATTAACAGCCAACTTAACAGCTGGACATCTCCTTATACAATTAATC
GCAACTGCGGCCTTTGTCCTTTTAACTATAATACCAACCGTGGCCTTACTAACCTCCCTA
GTCCTGTTCCTATTGACTATTTTAGAAGTGGCTGTAGCTATAATTCAAGCATACGTATTT
GTCCTTCTTTTAAGCTTATATCTACAAGAAAACGTATAA*
