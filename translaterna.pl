#!/usr/bin/perl
#translaterna.pl -- translate RNA nucleic acid sequence to protein sequence
#                  according to standard genetic code

#  set up table of standard genetic code
#  in the code provided, the line below is missing, making it unusable.
%standardgeneticcode = (
 "uuu"=> "Phe",   "ucu"=> "Ser", "uau"=> "Tyr",   "ugu"=> "Cys",
 "uuc"=> "Phe",   "ucc"=> "Ser", "uac"=> "Tyr",   "ugc"=> "Cys",
 "uua"=> "Leu",   "uca"=> "Ser", "uaa"=> "TER",   "uga"=> "TER",
 "uug"=> "Leu",   "ucg"=> "Ser", "uag"=> "TER",   "ugg"=> "Trp",
 "cuu"=> "Leu",   "ccu"=> "Pro", "cau"=> "His",   "cgu"=> "Arg",
 "cuc"=> "Leu",   "ccc"=> "Pro", "cac"=> "His",   "cgc"=> "Arg",
 "cua"=> "Leu",   "cca"=> "Pro", "caa"=> "Gln",   "cga"=> "Arg",
 "cug"=> "Leu",   "ccg"=> "Pro", "cag"=> "Gln",   "cgg"=> "Arg",
 "auu"=> "Ile",   "acu"=> "Thr", "aau"=> "Asn",   "agu"=> "Ser",
 "auc"=> "Ile",   "acc"=> "Thr", "aac"=> "Asn",   "agc"=> "Ser",
 "aua"=> "Ile",   "aca"=> "Thr", "aaa"=> "Lys",   "aga"=> "Arg",
 "aug"=> "Met",   "acg"=> "Thr", "aag"=> "Lys",   "agg"=> "Arg",
 "guu"=> "Val",   "gcu"=> "Ala", "gau"=> "Asp",   "ggu"=> "Gly",
 "guc"=> "Val",   "gcc"=> "Ala", "gac"=> "Asp",   "ggc"=> "Gly",
 "gua"=> "Val",   "gca"=> "Ala", "gaa"=> "Glu",   "gga"=> "Gly",
 "gug"=> "Val",   "gcg"=> "Ala", "gag"=> "Glu",   "ggg"=> "Gly"
);

#  process input data

while ($line = <DATA>) {                                  # read in line of input
     print "$line";                                       # transcribe to output
     chop();                                              # remove end-of-line character
     @triplets = unpack("a3" x (length($line)/3), $line); # pull out successive triplets
     foreach $codon (@triplets) {                         # loop over triplets
       print "$standardgeneticcode{$codon}";           # print out translation of each
     }                                                    # end loop on triplets
     print "\n\n";                                        # skip line on output
}                                                         # end loop on input lines}
#    what follows is input data
__END__
augcaucccuuuaau
ucugucuga
