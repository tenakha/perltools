#!/usr/bin/perl
#rna2dna.pl -- translate nucleic acid sequence to protein sequence
#                  according to standard genetic code


while ($line = <DATA>) {                                  # read in line of input
     print "$line";                                       # transcribe to output
     chop();                                              # remove end-of-line character
	 $line =~ tr/u/t/;
     print "$line";                                       # transcribe to output
     print "\n\n";                                        # skip line on output
}                                                         # end loop on input lines}
#    what follows is input data
__END__
augcaucccuuuaau
ucugucuga
